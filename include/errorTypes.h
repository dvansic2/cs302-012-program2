#pragma once
/*
 * Name: Demetri Van Sickle
 * Date: 04/27/2024
 * Class: CS302
 * Assignment: Program 2
 * Description:
 *      Contains error type definitions
 */

typedef struct
{
    int stringSize; //The size of the string that was passed in
} emptyString_t;

typedef struct 
{
    int obsticleType; //The type of obstacle that was passed in
} invalidObstacle_t;

typedef struct
{
    int participantType; //The type of participant that was passed in
} invalidParticipant_t;

typedef struct 
{
    
} nullData_t;

typedef struct
{
    int size; //The size of the list
} emptyList_t; 

typedef struct
{
    int actualSize; //Size the list was
    int expectedSize; //Size the list should be
} invalidListSize_t;

typedef struct
{

} raceUninitialized_t;

typedef struct 
{
  
} duplicateEntry_t;


