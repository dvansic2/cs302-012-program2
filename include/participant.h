#pragma once
/*
 * Name: Demetri Van Sickle
 * Date: 04/26/2024
 * Class: CS302
 * Assignment: Program 2
 * Description:
 *      Contains the class declarations that make up the Participant hierarchy
 */
#include <random>
#include <iostream>
#include <list>

enum obstacleType
{
    MAZE,
    HAILSTORM,
    HILL_CLIMB,
    NO_OBSTACLE,

    OBSTACLE_COUNT
};

enum participantType
{
    HUMAN, 
    BIRD,
    TURTLE,

    PARTICIPANT_COUNT
};

//********************************************************************************
//BEGIN Participant
//********************************************************************************
class Participant 
{
public:
    explicit Participant(participantType participant, const char * name); //I dont want to support overloading
    ~Participant();
    Participant(const Participant &participantToCopy);

    /*
     * Description:
     *      Returns the distance the participant has traveled
     * 
     * Arguments:
     *      None
     * 
     * Returns:
     *      int - the distance the participant has traveled
    */
    int getDistance() const;

    /*
     * Description:
     *      Returns if the participant is out of stamina
     * 
     * Arguments:
     *      None
     * 
     * Returns:
     *      true - the participant is out of stamina
     *      false - the participant is not out of stamina
    */
    bool outOfStamina() const;

    /*
     * Description:
     *      Moves the participant
     * 
     * Arguments:
     *      None
     * 
     * Returns:
     *      int - the distance the participant has moved
    */
    int move(obstacleType obstacle);

    /*
     * Description:
     *      Returns the name of the participant
     * 
     * Arguments:
     *      None
     * 
     * Returns:
     *      const char * - the name of the participant
    */
    const char * getName() const;

    /*
    * Description:
    *      Returns if the current participants stats are less than the given participants stats
    *      Compares stats total
    * 
    * Arguments:
    *      const Participant &- the participant to compare to
    * 
    * Returns:
    *      true - the current participants stats are less than the given participants stats
    *      false - the current participants stats are not less than the given participants stats
    */
    bool operator<(const Participant &op2) const;

    /*
    * Description:
    *      Returns if the current participants stats are greater than the given participants stats
    *      Compares stats total
    * 
    * Arguments:
    *      const Participant &- the participant to compare to
    * 
    * Returns:
    *      true - the current participants stats are greater than the given participants stats
    *      false - the current participants stats are not greater than the given participants stats
    */
    bool operator>(const Participant &op2) const;

    /*
     * Description:
     *      Returns if the current participants stats are less than or equal to the given participants stats
     *      Compares each stat
     * Arguments:
     *      const Participant & - the participant to compare to
     * 
     * Returns:
     *      true - the current participants stats are less than or equal to the given participants stats
     *      false - the current participants stats are not less than or equal to the given participants stats
    */
    bool operator<=(const Participant &op2) const;

    /*
     * Description:
     *      Returns if the current participants stats are greater than or equal to the given participants stats
     *      Compares each stat
     * 
     * Arguments:
     *      const Participant & - the participant to compare to
     * 
     * Returns:
     *      true - the current participants stats are greater than or equal to the given participants stats
     *      false - the current participants stats are not greater than or equal to the given participants stats
    */
    bool operator>=(const Participant &op2) const;

    /*
     * Description:
     *      Returns if the current participants stats and name are equal to the given participants stats
     *      Compares each stat
     * 
     * Arguments:
     *      const Participant & - the participant to compare to
     * 
     * Returns:
     *      true - the current participants stats are equal to the given participants stats
     *      false - the current participants stats are not equal to the given participants stats
    */
    bool operator==(const Participant &op2) const;

    /*
     * Description:
     *      Returns if the current participants stats and name are not equal to the given participants stats
     *      Compares each stat
     * 
     * Arguments:
     *      const Participant & - the participant to compare to
     * 
     * Returns:
     *      true - the current participants stats are not equal to the given participants stats
     *      false - the current participants stats are equal to the given participants stats
    */
    bool operator!=(const Participant &op2) const;

    /*
     * Description:
     *      Copies the given participant to the current participant
     * 
     * Arguments:
     *      participant & - the participant to copy
     * 
     * Returns:
     *      participant & - the current participant
    */
    Participant & operator=(const Participant &op2);

    //See definitions for descriptions
    friend std::ostream & operator<<(std::ostream &out, const Participant &participant);
    friend std::istream & operator>>(std::istream & in, Participant &participant);
    friend Participant operator+(const Participant & op1, const float op2);
    friend Participant operator+(const float op1, const Participant & op2);
    friend bool operator==(const std::string & op1, const Participant & op2);
    friend bool operator==(const Participant & op1, const std::string & op2);

    /*
     * Description:
     *      Adds the given float to the current participants stats
     * 
     * Arguments:
     *      const float & - the float to add to the current participants stats
     * 
     * Returns:
     *      Participant & - the current participant with the current participants stats plus the given float
    */
    Participant & operator+=(const float &op2);


protected:
    char *name;         //The name of the participant
    float speed;          //Deteimines how many units the participant can move in a turn (1 - 100)
    float armor;          //The amount of armor the participant has
    float intelligence;   //The amount of intelligence the participant has  (0 - 100) 
    float endurance;      //The amount of endurance the participant has (1 - 90)
    float stamina;        //The amount of stamina the participant has (50(starting) - 100)

    float speedBuff;          //Temporary boost to speed
    float armorBuff;          //Temporary boost to armor
    float intelligenceBuff;   //Temporary boost to intelligence
    float enduranceBuff;      //Temporary boost to endurance

    std::mt19937 eng; //Mersenne Twister engine
    std::uniform_int_distribution<> distr; //Uniform distribution

    /*
     * Description:
     *      Generates a random float between the given min and max
     * 
     * Arguments:
     *      float - the minimum float
     *      float - the maximum float
     * 
     * Returns:
     *      float - the random float (casted from an int)
    */
    float generateRandomFloat(int min, int max);
    
private:

    

    /*
     * Description:
     *      Ensures that the stats are within the correct range
     * 
     * Arguments:
     *      None
     * 
     * Returns:
     *      None
    */
    void validateStats();

    float distanceTravelled;  //The distance the participant has traveled
    obstacleType lastObstacleSeen; //The last obstacle the participant encountered
    int movesStaminaZero; //The number of moves the participant has made with 0 stamina
    std::list<int> obstaclesSeen;   //The obstacles the participant has seen
};

/*
 * Description:
 *      Outputs all the stats and name of the given participant
 *      Outputs a comma delimited list
 * 
 * Arguments:
 *      ostream & - the output stream
 *      const Participant & - the participant to output
 * 
 * Returns:
 *      ostream & - the output stream
*/
std::ostream & operator<<(std::ostream &out, const Participant &participant);

/*
 * Description:
 *      Inputs all the stats and name of the given participant
 * 
 * Arguments:
 *      istream & - the input stream
 *      const Participant & - the participant to input
 * 
 * Returns:
 *      istream & - the input stream
*/
std::istream & operator>>(std::istream & in, Participant &participant);

/*
    * Description:
    *      Adds the given float to the given participants stats
    * 
    * Arguments:
    *      const float & - the float to add to the given participants stats
    *      const Participant & - the participant to add the given float to
    * 
    * Returns:
    *      Participant - a new participant with the given participants stats plus the given float
*/
Participant operator+(const Participant & op1, const float op2);
Participant operator+(const float op1, const Participant & op2);

/*
 * Description:
 *      Returns if the given string is equal to the name of the given Participant
 * 
 * Arguments:
 *       const std::string & - the string to compare to the name of the given Participant
 *       const Participant & - the Participant to compare to the string
 * 
 * Returns:
 *      True - the string is equal to the name of the given Participant
 *      False - the string is not equal to the name of the given Participant
*/
bool operator==(const std::string & op1, const Participant & op2);
bool operator==(const Participant & op1, const std::string & op2);

//********************************************************************************
//END Participant
//********************************************************************************

//********************************************************************************
//BEGIN Human
//********************************************************************************
class Human : public Participant
{
public:
    Human(const char * name);
    ~Human();
    
    /*
     * Description:
     *     Activates the "think" powerup 
     * 
     * Arguments:
     *      None
     * 
     * Returns:
     *      None
    */
    void usePowerup();

    /*
     * Description:
     *     Activates the "rest" powerup 
     * 
     * Arguments:
     *      None
     * 
     * Returns:
     *      None
    */
    void rest();

    /*
     * Description:
     *     Moves the human
     * 
     * Arguments:
     *      obstacleType - the obstacle the human is facing
     * 
     * Returns:
     *      int - the distance the human has moved
    */
    int move(obstacleType obstacle);

private:
    bool inThought; //If the human is currently thinking
    bool isResting; //If the human is currently resting
    int numMovesThinking; //The number of moves the human has been thinking
    int numMovesResting; //The number of moves the human has been resting
};
//********************************************************************************
//END Human
//********************************************************************************

//********************************************************************************
//BEGIN Bird
//********************************************************************************
class Bird : public Participant
{
public:
    Bird(const char * name, const char * color);
    ~Bird();
    Bird(const Bird &birdToCopy);

    /*
     * Description:
     *      Turns on the glide ability if able
     * 
     * Arguments:
     *      None
     * 
     * Returns:
     *      None
    */
    void usePowerup();

    /*
     * Description:
     *      Moves the bird and accounts for powrups
     * 
     * Arguments:
     *      obstacleType - the obstacle the bird is facing
     * 
     * Returns:
     *      int - the distance the bird has moved
    */
    int move(obstacleType obstacle);

private:
    char *color; //The color of the bird
    int numFeathersLost; //The number of feathers the bird has lost
    bool inGlide; //If the bird is currently gliding
    int numMovesGliding;    //The number of moves the bird has been gliding

    void loseFeathers();
};
//********************************************************************************
//END Bird
//********************************************************************************

//********************************************************************************
//BEGIN Turtle
//********************************************************************************
class Turtle : public Participant
{
public:
    Turtle(const char * name);
    ~Turtle();

    /*
     * Description:
     *      Turns on the tank mode ability if able
     * 
     * Arguments:
     *      None
     * 
     * Returns:
     *      None
    */
    void usePowerup();

    /*
     * Description:
     *      Moves the turtle and accounts for powrups
     * 
     * Arguments:
     *      obstacleType - the obstacle the turtle is facing
     * 
     * Returns:
     *      int - the distance the turtle has moved
    */
    int move(obstacleType obstacle);

private:
    bool inTankMode; //If the turtle is currently in tank mode
    int numMovesInTankMode; //The number of moves the turtle has been in tank mode
    int shellCracks; //The number of times the turtles shell has cracked

    void crackShell();
};

//********************************************************************************
//END Turtle
//********************************************************************************