#pragma once
/*
 * Name: Demetri Van Sickle
 * Date: 05/01/2024
 * Class: CS302
 * Assignment: Program 2
 * Description:
 *      Contains the DLL class declaration
 */
#include <memory>
#include <utility>

//********************************************************************************
//BEGIN Node
//********************************************************************************
template<typename TYPE>
class Node
{
public:
    Node(std::unique_ptr<TYPE> data);
    ~Node();

    Node(const Node<TYPE> & nodeToCopy);

    /*
     * Description:
     *      Gets the pointer to the next node
     * 
     * Arguments:
     *      None
     * 
     * Returns:
     *      Node<TYPE> * - The pointer to the next node
     */
    Node<TYPE> * getNext() const;

    /*
     * Description:
     *      Gets the pointer to the previous node
     * 
     * Arguments:
     *      None
     * 
     * Returns:
     *      Node<TYPE> * - The pointer to the previous node
     */
    Node<TYPE> * getPrev() const;

    /*
     * Description:
     *      Sets the next pointer
     * 
     * Arguments:
     *      Node<TYPE> * - The next node
     * 
     * Returns:
     *      None
    */
    void setNext(Node<TYPE> * next);

    /*
     * Description:
     *      Sets the previous pointer
     * 
     * Arguments:
     *       Node<TYPE> * - The previous node
     * 
     * Returns:
     *      None
    */
    void setPrev(Node<TYPE> * prev);

    /*
     * Description:
     *      Return pointer to the data
     * 
     * Arguments:
     *      None
     * 
     * Returns:
     *      TYPE * - pointer to the data
     */
    TYPE * getData() const;

private:
    std::unique_ptr<TYPE> data;  //The data
    Node<TYPE> * next; //The next node
    Node<TYPE> * prev; //The previous node
};
//********************************************************************************
//END Node
//********************************************************************************


//********************************************************************************
//BEGIN DLL
//********************************************************************************
template<typename TYPE>
class DLL
{
public:
    DLL();
    ~DLL();

    DLL(const DLL<TYPE> & dllToCopy);

    /*
     * Description:
     *      Inserts a new TYPE object into the list
     *      Items are inserted from smallest to largest
     *      If the item is already in the list, it is not inserted
     * 
     *      Note: < <= operators must be defined for TYPE
     * 
     * Arguments:
     *      std::unique_ptr<TYPE> - The data to insert
     * 
     * Returns:
     *      None
    */
    void insert(std::unique_ptr<TYPE> data);

    /*
     * Description:
     *      Removes the item with the given name
     * 
     * Arguments:
     *      std::string & - The name of the item to remove
     * 
     * Returns:
     *      None
    */
    void remove(const std::string & name);

    /*
     * Description:
     *      Retrieves the item with the given name
     * 
     * Arguments:
     *      std::string & - The name of the item to retrieve
     * 
     * Returns:
     *      TYPE * - The pointer to the item
     *      nullptr - The item was not found
    */
    TYPE * retrieveData(const std::string & name) const;

    /*
     * Description:
     *      Gets the next item in the list
     *      This is used to iterate through the list
     *      
     *      Spot is lost if the list is modified
     * 
     * Arguments:
     *      None
     * 
     * Returns:
     *      TYPE * - The pointer to the next item
     *      nullptr - The end of the list has been reached
    */
    TYPE * getNext();

    /*
     * Description:
     *      Removes all items from the list
     * 
     * Arguments:
     *      None
     * 
     * Returns:
     *      None
    */
    void removeAll();

    /*
     * Description:
     *      Displays the list
     * 
     * Arguments:
     *      None
     * 
     * Returns:
     *      None
    */
    void display() const;

    /*
     * Description:
     *      Checks if the list is empty
     * 
     * Arguments:
     *      None
     * 
     * Returns:
     *      True - The list is empty
     *      False - The list is not empty
    */
    bool isEmpty() const;

private:
    Node<TYPE> * head; //The head of the list
    Node<TYPE> * tail; //The tail of the list
    int size; //The size of the list
    Node<TYPE> * current; //The current node

    /*
     * Description:
     *      Returns the node that the data should be inserted after
     * 
     * Arguments:
     *      Node<TYPE> * - The current node
     *      const TYPE * - The data to insert
     * 
     * Returns:
     *      Node<Type> * - The spot to insert the data
    */
    Node<TYPE> * findSpotR(Node<TYPE> * currentNode, const TYPE * data) const;

    /*
     * Description:
     *      Retruns the node with the given name
     * 
     * Arguments:
     *      const std::string & - name to look for
     *      Node<TYPE> * - The current node
     * 
     * Returns:
     *      Node<TYPE> * - The node with the given name
    */
    Node<TYPE> * retrieveR(const std::string & name, Node<TYPE> * currentNode) const;

    /*
     * Description:
     *      Retrieves the node with the given name
     * 
     * Arguments:
     *      std::string & - The name of the item to retrieve
     * 
     * Returns:
     *      TYPE * - The pointer to the item
     *      nullptr - The item was not found
    */
    Node<TYPE> * retrieveNode(const std::string & name) const;

    /*
     * Description:
     *      Displays the list
     * 
     * Arguments:
     *      Node<TYPE> * - The current node
     * 
     * Returns:
     *      None
    */
    void displayR(Node<TYPE> * currentNode) const;

    /*
     * Description:
     *      Removes all items from the list
     * 
     * Arguments:
     *      Node<TYPE> * - The current node
     * 
     * Returns:
     *     None 
    */
    void removeAllR(Node<TYPE> * currentNode);

    void copyR(Node<TYPE> * currentNode);
};
//********************************************************************************
//END DLL
//********************************************************************************

#include "../src/DLL.tpp"