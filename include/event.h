#pragma once
/*
 * Name: Demetri Van Sickle
 * Date: 05/05/2024
 * Class: CS302
 * Assignment: Program 2
 * Description:
 *      Contains the event class declarations
 */
#include "DLL.h"
#include "participant.h"

//********************************************************************************
//BEGIN event
//********************************************************************************
class Event
{
public:
    Event();
    ~Event();
    
    /*
     * Description:
     *      Runs the event
     * 
     * Arguments:
     *      None
     * 
     * Returns:
     *      None
     */
    void run();

    /*
     * Description:
     *      Wrapper for displayResultsCore
     * 
     * Arguments:
     *      None
     * 
     * Returns:
     *      None
     */
    void displayResults();

    /*
     * Description:
     *      Picks the racers for the event
     * 
     * Arguments:
     *      None
     * 
     * Returns:
     *      None
     */
    void pickRacers();

private:
    bool racersChosen; //Flag to determine if racers have been chosen
    participantType chosenParticipant; //The type of participant chosen
    DLL<Human> humanList; //List of human participants
    DLL<Bird> birdList; //List of bird participants
    DLL<Turtle> turtleList; //List of turtle participants

    std::mt19937 eng; //Mersenne Twister engine
    std::uniform_int_distribution<> distr; //Uniform distribution

    /*
     * Description:
     *      Starts the race simulation
     * 
     * Arguments:
     *      DLL<TYPE> & - reference to the list of participants
     * 
     * Returns:
     *     None 
    */
    template<typename TYPE>
    void startRaceSim(DLL<TYPE> & participantList);

    /*
     * Description:
     *      Clears the screen
     * 
     * Arguments:
     *      None
     * 
     * Returns:
     *      None
     */
    void clearScreen() const;

    /*
     * Description:
     *      Waits for the user to press enter
     * 
     * Arguments:
     *      None
     * 
     * Returns:
     *      None
     */
    void waitForEnter() const;

    /*
     * Description:
     *      Validates a number option
     * 
     * Arguments:
     *      const std::string & - The user input
     *      int - The max number of digits
     * 
     * Returns:
     *      int - The number
     */
    int validateNumOption(const std::string & userInput, int maxDigits) const;

    /*
     * Description:
     *      Adds a participant to the list
     * 
     * Arguments:
     *      const std::string & - The name of the participant
     *      participantType - The type of participant
     * 
     * Returns:
     *      None
     */
    void addParticipant(const std::string & name, const participantType type, const std::string & birdColor = "");

    /*
     * Description:
     *      Generates a random int between the given min and max
     * 
     * Arguments:
     *      int - the minimum int
     *      int - the maximum int
     * 
     * Returns:
     *      int - the random int (casted from an int)
    */
    int generateRandomInt(int min, int max);

    /*
     * Description:
     *      Rolls a "dice" and checks if the roll is successful
     * 
     * Arguments:
     *      float - chance of success (0 - 100)
     * 
     * Returns:
     *      true - check succeeded
     *      false - check failed
     */
    bool successCheck(float chance);

    /*
     * Description:
     *      Displays the results of the event
     * 
     * Arguments:
     *      None
     * 
     * Returns:
     *      None
     */
    template<typename TYPE>
    void displayResultsCore(DLL<TYPE> & participantList) const;

    /*
     * Description:
     *      Compares the postions two particpants for list sorting
     * 
     * Arguments:
     *      const TYPE * - The first participant
     *      const TYPE * - The second participant
     * 
     * Returns:
     *      true - the first goes before the second
     *      false - the second goes before the first
    */
    template<typename TYPE>
    bool positionComp(const TYPE * first, const TYPE * second) const;
};
//********************************************************************************
//END event
//********************************************************************************