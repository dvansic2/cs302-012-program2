/*
 * Name: Demetri Van Sickle
 * Date: 04/26/2024
 * Class: CS302
 * Assignment: Program 2
 * Description:
 *      Contains the class implementations for the Participant hierarchy
 */
#include <random>
#include "string.h"
#include "participant.h"
#include "errorTypes.h"

#define MAX_BUFFER_SIZE         (256 * 2)
#define DFLT_PARTICIPNT_NAME    "John Doe"
#define DFLT_BIRD_COLOR         "Blue"

#define STAMINA_COOLDOWN        3 //Nubmer of moves before stamina is resored
#define BASE_STAMINA_CONSUME    4 //Base amount of stamina consumed per move

#define MAZE_SUCCESS_TARGET     170
#define MAZE_FAILURE_PENALTY    -35

#define HILL_CLIMB_STAMINA_MULT 3

/*------------------------
Human Defines
--------------------------*/
#define HUMAN_THNK_DURATION     3
#define HUMAN_RST_DURATION      3
#define HUMAN_THNK_INTEL_BUFF   10
#define HUMAN_THNK_SPEED_DEBUFF -10
#define HUMAN_RST_STMA_RST_PCNT 0.75

/*------------------------
Bird Defines
--------------------------*/
#define BIRD_GLIDE_DURATION     3
#define BIRD_GLDE_STAMINA_BUFF  100
#define BIRD_GLDE_ARMOR_DEBUFF  -100
#define BIRD_MIN_FEATHERS_LOSE  1
#define BIRD_MAX_FEATHERS_LOSE  4

/*------------------------
Turtle Defines
--------------------------*/
#define TURTLE_TANK_DURATION    3
#define TURTLE_TANK_ARMOR_BUFF  100
#define TURTLE_MIN_SHELL_CRACKS 1
#define TURTLE_MAX_SHELL_CRACKS 4

/*------------------------
Stat MAX values
--------------------------*/
#define MAX_SPEED               100
#define MAX_ARMOR               100
#define MAX_INTELLIGENCE        100
#define MAX_ENDURANCE           100
#define MAX_STAMINA             100

/*------------------------
Stat MIN values
--------------------------*/
#define MIN_SPEED               0
#define MIN_ARMOR               1
#define MIN_INTELLIGENCE        0
#define MIN_ENDURANCE           1
#define MIN_STAMINA             0



//********************************************************************************
//BEGIN Participant
//********************************************************************************
Participant::Participant(participantType participant, const char * name) : 
    speedBuff(0), armorBuff(0), intelligenceBuff(0), enduranceBuff(0), distanceTravelled(0), lastObstacleSeen(NO_OBSTACLE), movesStaminaZero(0)
{
    std::random_device rd; //Seed for the random number generator
    eng = std::mt19937(rd()); //Initialize the engine with a random seed
    distr = std::uniform_int_distribution<>(-5, 5); //Distribution for the random number generator
    
    //Make sure name is not null
    if (name == nullptr || strlen(name) == 0)
    {
        //Set the name to the default name
        this->name = new char[strlen(DFLT_PARTICIPNT_NAME) + 1];
        strcpy(this->name, DFLT_PARTICIPNT_NAME);
    }
    else
    {
        //Allocate memory for new name and copy over the given name
        this->name = new char[strlen(name) + 1];
        strcpy(this->name, name);
    }

    //Set the stats based on the participant type
    switch (participant)
    {
    case HUMAN:
        speed = 20;
        armor = 25;
        intelligence = 70;
        endurance = 38;
        stamina = MAX_STAMINA;
        break;
    case BIRD:
        speed = 30;
        armor = 20;
        intelligence = 45;
        endurance = 69;
        stamina = MAX_STAMINA;
        break;
    case TURTLE:
        speed = 20;
        armor = 90;
        intelligence = 30;
        endurance = 75;
        stamina = MAX_STAMINA;
        break;
    default:
        invalidParticipant_t error = {
            .participantType = participant
        };
        throw error;
        break;
    }

    //Apply random alterations to the stats
    speed += distr(eng);
    armor += distr(eng);
    intelligence += distr(eng);
    endurance += distr(eng);
    stamina += distr(eng);

    validateStats();
}

Participant::~Participant()
{
    delete[] name;
}

Participant::Participant(const Participant &participantToCopy) : 
    speed(participantToCopy.speed), armor(participantToCopy.armor), intelligence(participantToCopy.intelligence), 
    endurance(participantToCopy.endurance), stamina(participantToCopy.stamina), speedBuff(participantToCopy.speedBuff), 
    armorBuff(participantToCopy.armorBuff), intelligenceBuff(participantToCopy.intelligenceBuff),
    enduranceBuff(participantToCopy.enduranceBuff), distanceTravelled(participantToCopy.distanceTravelled), lastObstacleSeen(participantToCopy.lastObstacleSeen)
{
    //Allocate and copy the new name
    name = new char[strlen(participantToCopy.name) + 1];
    strcpy(name, participantToCopy.name);

    //Copy the obstacles seen
    obstaclesSeen = participantToCopy.obstaclesSeen;

    //Copy the random number generator
    eng = participantToCopy.eng;
    distr = participantToCopy.distr;

    //Setup new random number generator
    std::random_device rd; //Seed for the random number generator
    eng = std::mt19937(rd()); //Initialize the engine with a random seed
}

void Participant::validateStats()
{
    //Make sure none of the stats are negative
    if(speed < MIN_SPEED)
    {
        speed = MIN_SPEED;
    }
    if(armor < MIN_ARMOR)
    {
        armor = MIN_ARMOR;
    }
    if(intelligence < MIN_INTELLIGENCE)
    {
        intelligence = MIN_INTELLIGENCE;
    }
    if(endurance < MIN_ENDURANCE)
    {
        endurance = MIN_ENDURANCE;
    }
    if(stamina < MIN_STAMINA)
    {
        stamina = MIN_STAMINA;
    }
    if(distanceTravelled < 0)
    {
        distanceTravelled = 0;
    }

    //Make sure none of the stats are above the max
    if(speed > MAX_SPEED)
    {
        speed = MAX_SPEED;
    }
    if(armor > MAX_ARMOR)
    {
        armor = MAX_ARMOR;
    }
    if(intelligence > MAX_INTELLIGENCE)
    {
        intelligence = MAX_INTELLIGENCE;
    }
    if(endurance > MAX_ENDURANCE)
    {
        endurance = MAX_ENDURANCE;
    }
    if(stamina > MAX_STAMINA)
    {
        stamina = MAX_STAMINA;
    }
}

const char * Participant::getName() const
{
    return name;
}

int Participant::getDistance() const
{
    return distanceTravelled;
}

bool Participant::outOfStamina() const
{
    return stamina <= 0;
}

int Participant::move(obstacleType obstacle)
{
    float buffedSpeed; //The speed of the participant after the buff
    float buffedArmor; //The armor of the participant after the buff
    float buffedIntelligence; //The intelligence of the participant after the buff
    float buffedEndurance; //The endurance of the participant after the buff

    float attempt; //The attempt to complete the obstacle
    float speedAdj; //Speed adjusted for the hailstorm

    //Restore the stamina if the cooldown is up
    if(movesStaminaZero >= STAMINA_COOLDOWN)
    {
        stamina = MAX_STAMINA;
        movesStaminaZero = 0;
    }

    //Apply the buffs
    buffedSpeed = speed + speedBuff;
    buffedArmor = armor + armorBuff;
    buffedIntelligence = intelligence + intelligenceBuff;
    buffedEndurance = endurance + enduranceBuff;

    //Make sure all the stats are within range
    validateStats();

    //If the stamina is 0, set the speed to the minimum
    if(stamina <= 0)
    {
        ++movesStaminaZero;
        buffedSpeed = MIN_SPEED;
    }

    //Determine how we move based on the obstacle
    switch(obstacle)
    {
    case MAZE:
        attempt = buffedIntelligence + generateRandomFloat(69, 150);

        if(attempt >= MAZE_SUCCESS_TARGET)
        {   
            distanceTravelled += speed;
        }
        else
        {
            distanceTravelled += MAZE_FAILURE_PENALTY;
        }

        //Decrese the stamina
        stamina -= BASE_STAMINA_CONSUME - (BASE_STAMINA_CONSUME * (buffedEndurance / 100));

        break;

    case HILL_CLIMB:
        //Travel the distance based on the speed
        distanceTravelled += buffedSpeed;

        //Decrese the stamina
        stamina -= (BASE_STAMINA_CONSUME * HILL_CLIMB_STAMINA_MULT) - (BASE_STAMINA_CONSUME * (buffedEndurance / 100));

        break;

    case HAILSTORM:
        //Dont change the speed if its already 0
        if(buffedSpeed != 0)
        {
            //Calculate the speed adjusted for the hailstorm
            speedAdj = buffedSpeed - ((1 / buffedArmor) * 10);

            if(speedAdj < 0)
            {
                speedAdj = MIN_SPEED;
            }
        }
        else
        {
            speedAdj = 0;
        }

        //Travel the distance based on the speed
        distanceTravelled += speedAdj;

        //Decrese the stamina
        stamina -= BASE_STAMINA_CONSUME - (BASE_STAMINA_CONSUME * (buffedEndurance / 100));

        break;

    case NO_OBSTACLE:
        //Travel the distance based on the speed
        distanceTravelled += buffedSpeed;

        //Decrese the stamina
        stamina -= 1 - (buffedEndurance / 10);

        break;
    default:
        invalidObstacle_t error = {
            .obsticleType = obstacle
        };
        throw error;
        break;
    }

    //Only record the obstacle if it is not NO_OBSTACLE AND it is not the same as the last obstacle
    if(obstacle != NO_OBSTACLE && obstacle != lastObstacleSeen)
    {
        //Add the obstacle to the list of obstacles seen
        obstaclesSeen.push_back(obstacle);
    }

    //Set the last obstacle to the current obstacle
    lastObstacleSeen = obstacle;

    //Make sure the distance travelled is not negative
    validateStats();

    //Return the distance travelled
    return distanceTravelled;
}

std::ostream & operator<<(std::ostream & out, const Participant & participant)
{
    out << participant.name << ", " << participant.distanceTravelled << ", " << participant.speed << ", " << participant.armor << ", " << participant.intelligence
        << ", " << participant.endurance << ", " << participant.stamina;

    return out;
}

std::istream & operator>>(std::istream & in, Participant & participant)
{
    char * buffer; //Buffer to hold the input
    char * token; //Pointer to hold the current token
    std::string statVal; //String to hold the stat value
    float * stats[] = 
        {
            &participant.speed,
            &participant.armor,
            &participant.intelligence,
            &participant.endurance,
            &participant.stamina
        }; //Array to hold the stats

    //Allocate buffer
    try
    {
        buffer = new char[MAX_BUFFER_SIZE];
    }
    catch(const std::exception& e)
    {
        throw e;
    }

    in.getline(buffer, MAX_BUFFER_SIZE);

    //Throw if the string is empty
    if(strlen(buffer) == 0)
    {
        emptyString_t error = {
            .stringSize = sizeof(buffer)
        };

        //Free the buffer
        delete[] buffer;

        throw error;
    }

    //Break up the buffer into tokens
    token = strtok(buffer, ",");

    //Dealoocate the old name
    delete[] participant.name;

    //Allocate and copy the new name
    try
    {
        participant.name = new char[strlen(token) + 1];
        strcpy(participant.name, token);
    }
    catch(const std::exception& e)
    {
        //Free the buffer
        delete[] buffer;

        std::cerr << e.what() << '\n';
        throw e;
    }

    //Assign each stat its respective value
    for(int index = 0; index < 5; ++index)
    {   
        //Get the next token
        token = strtok(NULL, ",");

        //Make sure there is a token, if not break
        if(token != NULL)
        {   
            //Convert the token to a float and assign it to the stat
            statVal = token;
            *stats[index] = stof(statVal);
        }
        else
        {
            break;
        }
    }

    //Free the buffer
    delete[] buffer;

    return in;
}

bool operator==(const std::string & op1, const Participant & op2)
{
    if(strcmp(op1.c_str(), op2.name) == 0)
    {
        return true;
    }
    else
    {
        return false;
    }
}

bool operator==(const Participant & op1, const std::string & op2)
{
    return op2 == op1;
}

Participant operator+(const Participant & op1, const float op2)
{
    //Create a temporary participant
    Participant temp(op1);

    //Add the float to the stats
    temp.speed += op2;
    temp.armor += op2;
    temp.intelligence += op2;
    temp.endurance += op2;
    temp.stamina += op2;

    return temp;
}

Participant operator+(const float op1, const Participant & op2)
{
    return op2 + op1;
}

bool Participant::operator<(const Participant &op2) const
{
    if(strcmp(this->name, op2.name) < 0)
    {
        return true;
    }
    else
    {
        return false;
    }
}

bool Participant::operator>(const Participant &op2) const
{
    if(strcmp(this->name, op2.name) > 0)
    {
        return true;
    }
    else
    {
        return false;
    }
}

bool Participant::operator<=(const Participant &op2) const
{
    if(strcmp(this->name, op2.name) < 0 || strcmp(this->name, op2.name) == 0)
    {
        return true;
    }
    else
    {
        return false;
    }
}

bool Participant::operator>=(const Participant &op2) const
{
    return !(*this <= op2);
}

bool Participant::operator==(const Participant &op2) const
{
    if(strcmp(this->name, op2.name) == 0)
    {
        return true;
    }
    else
    {
        return false;
    }
}

bool Participant::operator!=(const Participant &op2) const
{
    return !(*this == op2);
}

Participant & Participant::operator=(const Participant &op2)
{
    if(this != &op2)
    {
        //Dealocate the old name
        delete[] name;

        //Allocate and copy the new name
        name = new char[strlen(op2.name) + 1];
        strcpy(name, op2.name);

        //Copy over the stats
        speed = op2.speed;
        armor = op2.armor;
        intelligence = op2.intelligence;
        endurance = op2.endurance;
        stamina = op2.stamina;

        distanceTravelled = op2.distanceTravelled;

        obstaclesSeen = op2.obstaclesSeen;
    }

    return *this;
}

Participant & Participant::operator+=(const float &op2)
{
    speed += op2;
    armor += op2;
    intelligence += op2;
    endurance += op2;
    stamina += op2;

    return *this;
}

float Participant::generateRandomFloat(int min, int max)
{
    distr = std::uniform_int_distribution<>(min, max); //Distribution for the random number generator

    return static_cast<float>(distr(eng));
}

//********************************************************************************
//END Participant
//********************************************************************************


//********************************************************************************
//BEGIN Human
//********************************************************************************
Human::Human(const char * name) : Participant(HUMAN, name),
    inThought(false), isResting(false), numMovesThinking(0), numMovesResting(0)
{
}

Human::~Human()
{
}

void Human::usePowerup()
{
    //Dont activate if already thinking or if resting
    if(!inThought && !isResting)
    {
        //Set the human to be thinking
        inThought = true;
    }
}

void Human::rest()
{
    //Dont activate if already resting, or the stamina is above half
    if(!isResting && stamina <= (MAX_STAMINA / 2))
    {
        //Set the human to be resting
        isResting = true;
    }
}

int Human::move(obstacleType obstacle)
{
    //Set all the buffs to 0
    speedBuff = 0;
    armorBuff = 0;
    intelligenceBuff = 0;
    enduranceBuff = 0;

    //If the human is thinking, increase the number of moves thinking and apply the buffs/debuffs
    if(inThought)
    {
        ++numMovesThinking;

        //If the human has been thinking for HUMAN_THNK_DURATION moves, stop thinking
        if(numMovesThinking >= HUMAN_THNK_DURATION)
        {
            inThought = false;
            numMovesThinking = 0;
        }

        intelligenceBuff = HUMAN_THNK_INTEL_BUFF;
        speedBuff = HUMAN_THNK_SPEED_DEBUFF;
    }

    //If the human is resting, increase the number of moves resting and dont move
    if(isResting)
    {
        ++numMovesResting;

        //If the human has been resting for 3 moves, stop resting
        if(numMovesResting >= HUMAN_RST_DURATION)
        {
            isResting = false;
            numMovesResting = 0;

            //Restore the stamina to X% of the max
            stamina = MAX_STAMINA * HUMAN_RST_STMA_RST_PCNT;
        }

        speedBuff = -MAX_SPEED;
    }

    return Participant::move(obstacle);
}

//********************************************************************************
//END Human
//********************************************************************************


//********************************************************************************
//BEGIN Bird
//********************************************************************************
Bird::Bird(const char * name, const char * color) : Participant(BIRD, name), numFeathersLost(0), inGlide(false), numMovesGliding(0)
{
    //Make sure color is not null
    if (color == nullptr || strlen(color) == 0)
    {
        //Set the color to the default color
        this->color = new char[strlen(DFLT_BIRD_COLOR) + 1];
        strcpy(this->color, DFLT_BIRD_COLOR);
    }
    else
    {
        //Allocate memory for new color and copy over the given color
        this->color = new char[strlen(color) + 1];
        strcpy(this->color, color);
    }
}

Bird::~Bird()
{
    delete[] color;
}

Bird::Bird(const Bird &birdToCopy) : Participant(birdToCopy), numFeathersLost(birdToCopy.numFeathersLost), inGlide(birdToCopy.inGlide), 
            numMovesGliding(birdToCopy.numMovesGliding)
{
    //Allocate and copy the new color
    color = new char[strlen(birdToCopy.color) + 1];
    strcpy(color, birdToCopy.color);
}

void Bird::usePowerup()
{
    //Dont activate if already gliding
    if(!inGlide)
    {
        //Set the bird to be gliding
        inGlide = true;
    }
}

void Bird::loseFeathers()
{
    //Increase the number of feathers lost
    numFeathersLost += generateRandomFloat(BIRD_MIN_FEATHERS_LOSE, BIRD_MAX_FEATHERS_LOSE);
}

int Bird::move(obstacleType obstacle)
{
    //Set all the buffs to 0
    speedBuff = 0;
    armorBuff = 0;
    intelligenceBuff = 0;
    enduranceBuff = 0;

    //If the bird is gliding, increase the number of moves gliding and apply the buffs/debuffs
    if(inGlide)
    {
        ++numMovesGliding;

        //If the bird has been gliding for BIRD_GLIDE_DURATION moves, stop gliding
        if(numMovesGliding >= BIRD_GLIDE_DURATION)
        {
            inGlide = false;
            numMovesGliding = 0;
        }

        enduranceBuff = MAX_ENDURANCE;
        armorBuff = MIN_ARMOR;
    }

    //If the bird is in a hailstorm, lose feathers
    if(obstacle == HAILSTORM)
    {
        loseFeathers();
    }

    return Participant::move(obstacle);
}

//********************************************************************************
//END Bird
//********************************************************************************


//********************************************************************************
//BEGIN Turtle
//********************************************************************************
Turtle::Turtle(const char * name) : Participant(TURTLE, name), inTankMode(false), numMovesInTankMode(0), shellCracks(0)
{
}

Turtle::~Turtle()
{
}

void Turtle::usePowerup()
{
    //Dont activate if already in tank mode
    if(!inTankMode)
    {
        //Set the turtle to be in tank mode
        inTankMode = true;
    }
}

int Turtle::move(obstacleType obstacle)
{
    //Set all the buffs to 0
    speedBuff = 0;
    armorBuff = 0;
    intelligenceBuff = 0;
    enduranceBuff = 0;

    //If the turtle is in tank mode, increase the number of moves in tank mode and apply the buffs/debuffs
    if(inTankMode)
    {
        ++numMovesInTankMode;

        //If the turtle has been in tank mode for TURTLE_TANK_DURATION moves, stop tank mode
        if(numMovesInTankMode >= TURTLE_TANK_DURATION)
        {
            inTankMode = false;
            numMovesInTankMode = 0;
        }

        armorBuff = MAX_ARMOR;
        speedBuff = (speed - 3) * -1;
    }

    return Participant::move(obstacle);
}

void Turtle::crackShell()
{
    //Increase the number of shell cracks randomly
    shellCracks += generateRandomFloat(TURTLE_MIN_SHELL_CRACKS, TURTLE_MAX_SHELL_CRACKS);
}
//********************************************************************************
//END Turtle
//********************************************************************************