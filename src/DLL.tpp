/*
 * Name: Demetri Van Sickle
 * Date: 05/01/2024
 * Class: CS302
 * Assignment: Program 2
 * Description:
 *      Contains the DLL class implementation
 */
#ifndef DLL_CPP
#define DLL_CPP

#include <iostream>
#include <string>
#include "DLL.h"
#include "errorTypes.h"

//********************************************************************************
//BEGIN Node
//********************************************************************************
template<typename TYPE>
Node<TYPE>::Node(std::unique_ptr<TYPE> data) : data(std::move(data)), next(nullptr), prev(nullptr)
{
}

template<typename TYPE>
Node<TYPE>::~Node()
{
    data.reset();
}

template<typename TYPE>
Node<TYPE>::Node(const Node<TYPE> & nodeToCopy) : next(nullptr), prev(nullptr)
{   
    std::unique_ptr<TYPE> newData; //The new data object

    //Check if the data is null
    if(nodeToCopy.getData() == nullptr)
    {
        nullData_t error;
        throw error;
    }

    //Make a new data object
    newData = std::make_unique<TYPE>(*nodeToCopy.getData());

    //Set the data
    this->data = std::move(newData);
}

template<typename TYPE>
void Node<TYPE>::setNext(Node<TYPE> * next)
{
    this->next = next;
}

template<typename TYPE>
void Node<TYPE>::setPrev(Node<TYPE> * prev)
{
    this->prev = prev;
}

template<typename TYPE>
Node<TYPE> * Node<TYPE>::getNext() const
{
    return next;
}

template<typename TYPE>
Node<TYPE> * Node<TYPE>::getPrev() const
{
    return prev;
}

template<typename TYPE>
TYPE * Node<TYPE>::getData() const
{
    return data.get();
}
//********************************************************************************
//END Node
//********************************************************************************


//********************************************************************************
//BEGIN DLL
//********************************************************************************
template<typename TYPE>
DLL<TYPE>::DLL() : head(nullptr), tail(nullptr), size(0), current(head)
{
}

template<typename TYPE>
DLL<TYPE>::~DLL()
{
    removeAllR(head);
}

template<typename TYPE>
DLL<TYPE>::DLL(const DLL<TYPE> & dllToCopy) : head(nullptr), tail(nullptr), size(0), current(head)
{
    //Check if the head is null
    if(dllToCopy.head == nullptr)
    {
        nullData_t error;
        throw error;
    }

    copyR(dllToCopy.head);
}

template<typename TYPE>
void DLL<TYPE>::insert(std::unique_ptr<TYPE> data)
{
    Node<TYPE> * newNode; //The new node
    Node<TYPE> * spotNode; //The spot node where the new node will be inserted

    //Check if the data is null
    if(data == nullptr)
    {
        nullData_t error;
        throw error;
    }

    //Create a new node
    newNode = new Node<TYPE>(std::move(data));

    //If the list is empty
    if(head == nullptr)
    {   
        //Set the head and tail to the new node
        head = newNode;
        tail = newNode;

        //Increment the size
        ++size;

    }
    //If data is less than the first item in the list
    else if(*newNode->getData() <= *head->getData())
    {
        std::string name = newNode->getData()->getName();

        //Make sure we are not inserting a duplicate
        if(retrieveData(name) != nullptr)
        {
            delete newNode;
            duplicateEntry_t error;
            throw error;
        }

        //Set the old first node's prev ptr to the new node
        head->setPrev(newNode);

        //Set the new node's next ptr to the old first node
        newNode->setNext(head);

        //Set the head to the new node
        head = newNode;

        //Increment the size
        ++size;
    }
    else
    {
        //Find the node to insert the new node after
        spotNode = findSpotR(head, newNode->getData());

        std::string name = newNode->getData()->getName();

        //Make sure we are not inserting a duplicate
        if(retrieveData(name) != nullptr)
        {
            delete newNode;
            duplicateEntry_t error;
            throw error;
        }

        //Determine if this spotNode is the tail
        if(spotNode == tail)
        {
            //Set the new node's prev ptr to the current spotNode
            newNode->setPrev(spotNode);

            //Set the current spotNode's next ptr to the new node
            spotNode->setNext(newNode);

            //Set the tail to the new node
            tail = newNode;

            //Increment the size
            ++size;
        }
        else
        {
            //Set the new node's prev ptr to the current spotNode
            newNode->setPrev(spotNode);

            //Set the new node's next ptr to the current spotNode's next ptr
            newNode->setNext(spotNode->getNext());

            //Set the current spotNode's next ptr to the new node
            spotNode->setNext(newNode);

            //Increment the size
            ++size;
        }
    }

    //Reset the current node pointer
    current = head;
}

template<typename TYPE>
TYPE * DLL<TYPE>::retrieveData(const std::string & name) const  
{
    Node<TYPE> * foundNode; //The found node

    //Make sure string is not empty
    if(name.empty())
    {
        emptyString_t error;
        error.stringSize = name.size();
        throw error;
    }

    //Check if the list is empty
    if(isEmpty())
    {
        emptyList_t error;
        error.size = size;
        throw error;
    }

    //Find node with matching data
    foundNode = retrieveNode(name);

    //Return pointer to the data if found
    if(foundNode == nullptr)
    {
        return nullptr;
    }
    else
    {
        return foundNode->getData();
    }
}

template<typename TYPE>
Node<TYPE> * DLL<TYPE>::retrieveNode(const std::string & name) const  
{
    Node<TYPE> * foundNode; //The found node

    //Make sure string is not empty
    if(name.empty())
    {
        emptyString_t error;
        error.stringSize = name.size();
        throw error;
    }

    //Check if the list is empty
    if(isEmpty())
    {
        emptyList_t error;
        error.size = size;
        throw error;
    }

    //Find node with matching data
    foundNode = retrieveR(name, head);

    //Return pointer to the data if found
    return foundNode;
}

template<typename TYPE>
void DLL<TYPE>::remove(const std::string & name)
{
    Node<TYPE> * foundNode; //The found node
    Node<TYPE> * prevNode; //The node before the foundnode

    //Make sure string is not empty
    if(name.empty())
    {
        emptyString_t error;
        error.stringSize = name.size();
        throw error;
    }

    //Check if the list is empty
    if(isEmpty())
    {
        emptyList_t error;
        error.size = size;
        throw error;
    }

    //Get the node to remove
    foundNode = retrieveNode(name);

    //Proceed to remove node if it was found
    if(foundNode != nullptr)
    {
        //Node is the only item
        if(foundNode == head && foundNode == tail)
        {
            //Delete the node
            delete foundNode;

            //Reset head and tail
            head = nullptr;
            tail = nullptr;

            //Decrement the size
            --size;

            //Size should be zero here, if its not we have an issue
            if(size != 0)  //TODO: change to assert
            {
                invalidListSize_t error;
                error.actualSize = size;
                error.expectedSize = 0;
                throw error;
            }
        }

        //Node is the head
        else if(foundNode == head)
        {
            //Set head to the new first node
            head = foundNode->getNext();

            //Reset the new head's prev
            head->setPrev(nullptr);

            //Delete the node
            delete foundNode;

            //Decrement the size
            --size;
        }
        
        //Node is at the end
        else if(foundNode == tail)
        {
            //Point tail to foundNode's prev
            tail = foundNode->getPrev();

            //Reset tail's next
            tail->setNext(nullptr);

            //Delete the node
            delete foundNode;

            //Decrement the size
            --size;
        }

        //Node is in the middle
        else if(foundNode->getNext() != nullptr && foundNode->getPrev() != nullptr)
        {
            prevNode = foundNode->getPrev();

            //Set the prev node to point to the new next
            prevNode->setNext(foundNode->getNext());

            //Set the new next's prev to the prev
            foundNode->getNext()->setPrev(prevNode);

            //Delete the node
            delete foundNode;

            //Decrement the size
            --size;
        }

        //Reset the current node pointer
        current = head;

    }
}

template<typename TYPE>
void DLL<TYPE>::removeAll()
{
    removeAllR(head);

    //Reset the head and tail
    head = nullptr;
    tail = nullptr;

    //Reset the size
    size = 0;

    //Reset the current node pointer
    current = head;
}

template<typename TYPE>
void DLL<TYPE>::display() const
{
    //Display the list
    displayR(head);
}

template<typename TYPE>
void DLL<TYPE>::displayR(Node<TYPE> * currentNode) const
{
    //Base case
    if(currentNode == nullptr)
    {
        return;
    }

    //Display the current node
    std::cout << *currentNode->getData() << std::endl;

    //Keep going
    displayR(currentNode->getNext());
}

template<typename TYPE>
bool DLL<TYPE>::isEmpty() const
{
    return(head == nullptr);
}

template<typename TYPE>
TYPE * DLL<TYPE>::getNext()
{
    TYPE * dataToReturn;

    if(isEmpty())
    {
        emptyList_t error;
        error.size = size;
        throw error;
    }

    //If we have reached the end
    if(current == nullptr)
    {   
        //Reset current back to begining
        current = head;

        return nullptr;
    }

    //Get the data to return
    dataToReturn = current->getData();

    //Increment the current node pointer
    current = current->getNext();
    
    return dataToReturn;
}

template<typename TYPE>
Node<TYPE> * DLL<TYPE>::retrieveR(const std::string & name, Node<TYPE> * currentNode) const
{
    //Base case
    if(currentNode == nullptr)
    {
        return nullptr;
    }

    if(*currentNode->getData() == name)
    {
        return currentNode;
    }

    return retrieveR(name, currentNode->getNext());
}

template<typename TYPE>
Node<TYPE> * DLL<TYPE>::findSpotR(Node<TYPE> * currentNode, const TYPE * data) const
{
    //Base case
    if(currentNode->getNext() == nullptr)
    {
        //Find if we need to inert to the left or right of the current node
        if(*currentNode->getData() < *data)
        {
            return currentNode;
        }
        else
        {
            return currentNode->getPrev();
        }
    }

    //Check to see if the current node data is less than the data to insert
    if(*currentNode->getData() < *data)
    {
        //Keep going
        return findSpotR(currentNode->getNext(), data);
    }
    else
    {
        //Return the current node
        return currentNode->getPrev();
    }
}

template<typename TYPE>
void DLL<TYPE>::removeAllR(Node<TYPE> * currentNode)
{
    //Base case
    if(currentNode == nullptr)
    {
        return;
    }

    //Keep going
    removeAllR(currentNode->getNext());

    //Delete the current node
    delete currentNode;
}

template<typename TYPE>
void DLL<TYPE>::copyR(Node<TYPE> * currentNode)
{
    //Base case
    if(currentNode == nullptr)
    {
        return;
    
    }

    //Create new node
    Node<TYPE> * newNode = new Node<TYPE>(*currentNode);

    //If this is the first entry 
    if(head == nullptr)
    {
        head = newNode;
        tail = newNode;
    }
    //For all other entries
    else
    {   
        //Set the last entry to point to new node
        tail->setNext(newNode);

        //Set the new node's prev to point to the old last node
        newNode->setPrev(tail);

        //Set tail to point to new node
        tail = newNode;
    }

    copyR(currentNode->getNext());
}

//********************************************************************************
//END DLL
//********************************************************************************
#endif