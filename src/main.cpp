/*
 * Name: Demetri Van Sickle
 * Date: 05/08/2024
 * Class: CS302
 * Assignment: Program 2
 * Description:
 *      Runs the program
 */
#include "event.h"

int main()
{
    Event mainEvent;

    mainEvent.pickRacers();

    mainEvent.run();

    mainEvent.displayResults();

    return 0;
}