/*
 * Name: Demetri Van Sickle
 * Date: 04/27/2024
 * Class: CS302
 * Assignment: Program 2
 * Description:
 *      NOT FOR GRADING - This is a test file to test the functionality of the code
 */
// #include <iostream>
// #include "participant.h"
// #include "DLL.h"
// #include "event.h"

// int main()
// {
//     // Participant p1(HUMAN, "John Doe");
//     // Participant p2(HUMAN, "Bill");

//     // std::string test;

//     // std::cout << p1 << std::endl;
//     // std::cout << p2 << std::endl;

//     // // std::cout << "Enter stats: ";

//     // // std::cin >> p1;

//     // std::cout << p1 << std::endl;

//     // if(p1 < p2)
//     // {
//     //     std::cout << "p1 is less than p2" << std::endl;
//     // }
//     // else if(p1 > p2)
//     // {
//     //     std::cout << "p1 is greater than p2" << std::endl;
//     // }
//     // else
//     // {
//     //     std::cout << "p1 is equal to p2" << std::endl;
//     // }

//     // if(p1 <= p2)
//     // {
//     //     std::cout << "p1 is less than or equal to p2" << std::endl;
//     // }
//     // else if(p1 >= p2)
//     // {
//     //     std::cout << "p1 is greater than or equal to p2" << std::endl;
//     // }
//     // else
//     // {
//     //     std::cout << "p1 is not less than or equal to p2" << std::endl;
//     // }

//     // p1 = p2;

//     // std::cout << p1 << std::endl;
//     // std::cout << p2 << std::endl;

//     // if(p1 == p2)
//     // {
//     //     std::cout << "p1 is equal to p2" << std::endl;
//     // }

//     // Participant p3(p1);

//     // std::cout << p1 << std::endl;
//     // std::cout << p3 << std::endl;

//     // p1 = p3 + 5.5;

//     // std::cout << p1 << std::endl;

//     // Turtle h1("Billy bob");

//     // std::cout << h1 << std::endl;

//     // std::mt19937 eng; //Mersenne Twister engine
//     // std::uniform_int_distribution<> distr; //Uniform distribution
//     // std::random_device rd; //Seed for the random number generator
//     // eng = std::mt19937(rd()); //Initialize the engine with a random seed
//     // distr = std::uniform_int_distribution<>(0, 3); //Distribution for the random number generator

//     // bool inObstacle = false;
//     // int obstacle = 3;
//     // int obstacleMoves = 0;

//     // // // h1.think();
//     // // for(int i = 0; i < 50000; i++)
//     // // {
//     // //     if(h1.outOfStamina())
//     // //     {
//     // //         std::cout << "Out of stamina" << std::endl;
//     // //     }

//     // //     if(inObstacle)
//     // //     {
//     // //         if(obstacleMoves >= 4)
//     // //         {
//     // //             inObstacle = false;
//     // //             obstacle = 3;
//     // //             obstacleMoves = 0;
//     // //         }
//     // //         else
//     // //         {
//     // //             ++obstacleMoves;
//     // //         }
//     // //     }
//     // //     else
//     // //     {
//     // //         distr = std::uniform_int_distribution<>(1, 100);
//     // //         if(distr(eng) >= 70)
//     // //         {
//     // //             distr = std::uniform_int_distribution<>(0, 3);
//     // //             inObstacle = true;
//     // //             obstacle = distr(eng);
//     // //             obstacleMoves = 0;
//     // //             std::cout << "Obstacle: " << obstacle << std::endl;
//     // //         }
//     // //     }

//     // //     std::cout << h1.move(static_cast<obstacleType>(obstacle)) << std::endl;
//     // // }

//     // // std::cout << h1 << std::endl;

//     // std::string name = "Goat";

//     // std::unique_ptr<Turtle> obj1 = std::make_unique<Turtle>(name.c_str());

//     // Node<Turtle> node1(std::move(obj1));

//     // std::cout << *node1.getData() << std::endl;

//     // if(node1.getPrev() == nullptr)
//     // {
//     //     std::cout << "Prev is null" << std::endl;
//     // }

//     // // Node<Turtle> node2(std::make_unique<Turtle>("Cheese"));

//     // // std::cout << "____" << *node1.getNext()->getData() << std::endl;


//     // DLL<Turtle> list;

//     // list.insert(std::make_unique<Turtle>("Z"));
//     // list.insert(std::make_unique<Turtle>("Y"));
//     // // std::cout << "Doing: D" << std::endl;
//     // list.insert(std::make_unique<Turtle>("Y"));
//     // // std::cout << "Doing: f" << std::endl;
//     // list.insert(std::make_unique<Turtle>("C"));
//     // // std::cout << "Doing: F" << std::endl;
//     // list.insert(std::make_unique<Turtle>("C"));
//     // list.insert(std::make_unique<Turtle>("!"));
//     // list.insert(std::make_unique<Turtle>("ss"));
//     // list.insert(std::make_unique<Turtle>("C"));

//     // Turtle * turt = list.retrieveData("ss");
//     // std::cout << *turt << "\n" << std::endl;
//     // // list.retrieve("");
//     // list.display();
//     // list.remove("Z");
  
//     // std::cout << *turt << std::endl;

//     // // list.retrieveData("C");
//     // std::cout << "  "<< std::endl;

//     // Turtle * turt2 = list.getNext();

//     // do
//     // {
//     //    std::cout << "__: " << *turt2 << std::endl;

//     //    turt2 = list.getNext();
//     // } while (turt2 != nullptr);
    

//     // list.display();

//     // list.removeAll();

//     Event mainEvent;

//     mainEvent.pickRacers();

//     mainEvent.run();

//     return 0;
// }