/*
 * Name: Demetri Van Sickle
 * Date: 05/05/2024
 * Class: CS302
 * Assignment: Program 2
 * Description:
 *      Contains the DLL class implementation
 */

#include <chrono>
#include <thread>
#include <iostream>
#include <climits>
#include "event.h"
#include "errorTypes.h"

#define RACE_LENGTH             60000.0f
#define DEFAULT_PARTICIPANT     (HUMAN)
#define DFLT_PARTICIPANT_CNT    3
#define MAX_PARTICIPANTS        10
#define MIN_PARTICIPANTS        1

#define OBSTACLE_DURATION       4
#define OBSTACLE_CHANCE         44

#define POWERUP_CHANCE          33

#define PRINT_DELAY_MS          3


Event::Event() : racersChosen(false), chosenParticipant(HUMAN)
{
    std::random_device rd; //Seed for the random number generator
    eng = std::mt19937(rd()); //Initialize the engine with a random seed
    distr = std::uniform_int_distribution<>(-5, 5); //Distribution for the random number generator
}

Event::~Event()
{
}

void Event::pickRacers()
{
    bool validInput = false; //Flag to determine if input is valid
    std::string userInput; //User input
    int userChoice; //User choice
    std::string birdColor; //Bird color

    //Get participant type
    while (!validInput)
    {
        clearScreen();

        std::cout << "Pick the type of participant you want: " << std::endl;
        std::cout << "0. Human" << std::endl;
        std::cout << "1. Bird" << std::endl;
        std::cout << "2. Turtle" << std::endl;
        
        try
        {
            //Get user input
            std::cin >> userInput;

            //Validate user input
            userChoice = validateNumOption(userInput, 1);

            if (userChoice == INT_MIN || userChoice < HUMAN || userChoice > PARTICIPANT_COUNT - 1)
            {
                throw std::invalid_argument("Invalid input");
            }

            chosenParticipant = static_cast<participantType>(userChoice);

            validInput = true;
        }
        catch(...)
        {
            std::cin.clear();
            std::cout << "\n\nInvalid input" << std::endl;
            waitForEnter();
        }
    } 

    //Add the participants
    validInput = false;
    while (!validInput)
    {
        clearScreen();
        
        try
        {
            std::cout << "Pick the number of participants you want: " << std::endl;

            std::cin >> userChoice;

            if ( userChoice < MIN_PARTICIPANTS || userChoice > MAX_PARTICIPANTS)
            {
                throw std::invalid_argument("Invalid input");
            }

            for(int i = 0; i < userChoice; ++i)
            {
                clearScreen();

                std::cout << "Enter the name of participant " << i + 1 << ": ";
                std::cin >> userInput;

                if(chosenParticipant == BIRD)
                {
                    std::cout << "Enter the color of the bird: ";
                    std::cin >> birdColor;
                    addParticipant(userInput, chosenParticipant, birdColor);
                }
                else
                {
                    addParticipant(userInput, chosenParticipant);
                }
            }
            validInput = true;
        }
        catch(duplicateEntry_t)
        {
            std::cin.clear();
            std::cout << "[" << userInput << "] is already in the list" << std::endl;
            std::cout << "Please start again" << std::endl;

            switch(chosenParticipant)
            {
                case HUMAN:
                    humanList.removeAll();
                    break;
                case BIRD:
                    birdList.removeAll();
                    break;
                case TURTLE:
                    turtleList.removeAll();
                    break;
                default:
                    throw invalidParticipant_t{chosenParticipant};
                    break;
            }
            
            waitForEnter();
        }
        catch(...)
        {
            std::cin.clear();
            std::cout << "\n\nInvalid input" << std::endl;
            waitForEnter();
        }
    } 

    racersChosen = true;
}

void Event::run()
{
    std::cout << "Running event.." << std::endl;
    try
    {
        switch (chosenParticipant)
        {
        case HUMAN:
            startRaceSim<Human>(humanList);
            break;
        case BIRD:
            startRaceSim<Bird>(birdList);
            break;
        case TURTLE:
            startRaceSim<Turtle>(turtleList);
            break;
        default:
            throw invalidParticipant_t{chosenParticipant};
            break;
        }
        
    }
    catch(raceUninitialized_t)
    {
        std::cerr << "Race uninitalized, using defaults" << std::endl;

        chosenParticipant = DEFAULT_PARTICIPANT;

        //Make default participants
        for(int i = 0; i < DFLT_PARTICIPANT_CNT; ++i)
        {
            std::string participantName = "Participant " + std::to_string(i + 1);

            addParticipant(participantName, chosenParticipant);
        }

        racersChosen = true;

        switch (chosenParticipant)
        {
        case HUMAN:
            startRaceSim<Human>(humanList);
            break;
        case BIRD:
            startRaceSim<Bird>(birdList);
            break;
        case TURTLE:
            startRaceSim<Turtle>(turtleList);
            break;
        default:
            throw invalidParticipant_t{chosenParticipant};
            break;
        }
    }
}

void Event::displayResults()
{
    std::cout << "Displaying results:\n" << std::endl;
    std::cout << "Name | Distance Traveled | Speed | Armor | Intelligence | Endurance | Stamina Left\n" << std::endl;
    
    switch(chosenParticipant)
    {
    case HUMAN:
        displayResultsCore<Human>(humanList);
        break;
    case BIRD:
        displayResultsCore<Bird>(birdList);
        break;
    case TURTLE:
        displayResultsCore<Turtle>(turtleList);
        break;
    default:
        throw invalidParticipant_t{chosenParticipant};
        break;
    }
}

template<typename TYPE>
void Event::startRaceSim(DLL<TYPE> & participantList)
{
    bool raceOver = false; //Flag to determine if the race is over
    int obstacleNumMoves = 0; //Number of moves the obstacle has been active
    obstacleType currentObstacle = NO_OBSTACLE; //The current obstacle
    TYPE * participantPtr; //Pointer to the current participant
    int diceRoll; //The roll of the "dice"
    int amountMoved; //ammount participant moved

    if(!racersChosen)
    {
        raceUninitialized_t error;
        throw error;
    }

    //Main simulation loop
    while(!raceOver)
    {
        //Detrimine if an obstacle will be encountered (only if there isnt one already happening)
        if(currentObstacle == NO_OBSTACLE)
        {   
            //If the dice roll is within the obstacle chance
            if(successCheck(OBSTACLE_CHANCE))
            {   
                //Decide what the obstacle will be
                diceRoll = generateRandomInt(0, 3);
                currentObstacle = static_cast<obstacleType>(diceRoll);
                obstacleNumMoves = 0;
            }
        }
        //Else keep track of the current obstacle
        else
        {   
            //If the obstacle has run its course
            if(obstacleNumMoves >= OBSTACLE_DURATION)
            {
                //Reset the obstacle
                obstacleNumMoves = 0;
                currentObstacle = NO_OBSTACLE;
            }
            else
            {
                ++obstacleNumMoves;
            }
        }
        
        //Cycle through each participant and move
        participantPtr = participantList.getNext();
        while(participantPtr != nullptr)
        {
            //Detrimine if power ups are allowed and activate it
            if(successCheck(POWERUP_CHANCE))
            {
                participantPtr->usePowerup();
            }

            //Move the participant
            amountMoved = participantPtr->move(currentObstacle);

            //Print out current progress
            std::cout << participantPtr->getName() << "'s progress: " << (amountMoved / RACE_LENGTH) * 100 << "%" << std::endl;

            //Get next participant
            participantPtr = participantList.getNext();
        }

        //See if any of the racers have finished
        participantPtr = participantList.getNext();
        while(participantPtr != nullptr)
        {
            if(participantPtr->getDistance() >= RACE_LENGTH)
            {
                raceOver = true;
            }

            participantPtr = participantList.getNext();
        }
        
        std::this_thread::sleep_for(std::chrono::milliseconds(PRINT_DELAY_MS));
        clearScreen();
    }

}

void Event::clearScreen() const
{
    //Clear screen
    std::cout << "\033[2J\033[1;1H";
}

void Event::waitForEnter() const
{
    //Wait for user to press enter
    std::cout << "Press enter to continue..." << std::endl;
    std::cin.ignore(INT_MAX, '\n'); //This is ignoring all to the newline character, helps in cin.fails later in the program
    std::cin.get();
}

int Event::validateNumOption(const std::string & userInput, int maxDigits) const
{
    //Make sure number of digits is valid
    if (static_cast<int>(userInput.length()) > maxDigits)
    {
        return INT_MIN;
    }

    //Make sure input is a number
    for(int i = 0; i < static_cast<int>(userInput.length()); ++i)
    {
        if (std::isdigit(userInput[i]) == false)
        {
            return INT_MIN;
        }
    }
    
    return std::stoi(userInput);
}

void Event::addParticipant(const std::string & name, participantType type, const std::string & birdColor)
{
    switch(type)
    {
        case HUMAN:
            humanList.insert(std::make_unique<Human>(name.c_str()));
            break;
        case BIRD:
            birdList.insert(std::make_unique<Bird>(name.c_str(), birdColor.c_str()));
            break;
        case TURTLE:
            turtleList.insert(std::make_unique<Turtle>(name.c_str()));
            break;
        default:
            throw invalidParticipant_t{type};
            break;
    }
}

int Event::generateRandomInt(int min, int max)
{
    distr = std::uniform_int_distribution<>(min, max); //Distribution for the random number generator

    return distr(eng);
}

bool Event::successCheck(float chance)
{
    int diceRoll; //The roll of the "dice"

    //Roll the dice
    diceRoll = generateRandomInt(0, 100);

    //If the dice roll is within the chance
    if(diceRoll >= 100 - chance)
    {   
        return true;
    }
    else
    {
        return false;
    }
}

template<typename TYPE>
void Event::displayResultsCore(DLL<TYPE> & participantList) const
{
    TYPE * participantPtr; //Pointer to the current participant
    std::list<TYPE *> participantPositions; //Participants postions sorted for their finishing position
    int place = 1;

    //Sort participants by distance traveled (ie 1st, 2nd.. place)
    participantPtr = participantList.getNext();
    while(participantPtr != nullptr)
    {
        participantPositions.push_front(participantPtr);

        participantPtr = participantList.getNext();
    }

    //Sort
    participantPositions.sort(
            [this](const TYPE * first, const TYPE * second) 
            {
                return positionComp(first, second);
            }
        );

    //Print out the results
    for(typename std::list<TYPE *>::const_iterator it = participantPositions.begin(); it != participantPositions.end(); ++it)
    {
        std::cout << "Place #" << place << ": " << *(*it) << std::endl;

        ++place;
    }
}

template<typename TYPE>
bool Event::positionComp(const TYPE * first, const TYPE * second) const
{
    if(first == nullptr || second == nullptr)
    {
        return false;
    }

    if(first->getDistance() > second->getDistance())
    {
        return true;
    }
    else
    {
        return false;
    }
}