# Compiler settings
CXX = g++
CXXFLAGS = -std=c++17 -Wall -g
LDFLAGS =

# Build settings
SRC_DIR = src
INC_DIR = include
OBJ_DIR = obj
BIN_DIR = bin

SOURCES = $(wildcard $(SRC_DIR)/*.cpp)
OBJECTS = $(SOURCES:$(SRC_DIR)/%.cpp=$(OBJ_DIR)/%.o)
EXECUTABLE = $(BIN_DIR)/program2

# Default make
all: build $(EXECUTABLE)

# Build the directories
build:
	@mkdir -p $(OBJ_DIR) $(BIN_DIR)

# Linking all the object files to make the executable
$(EXECUTABLE): $(OBJECTS)
	$(CXX) $^ -o $@ $(LDFLAGS)

# Compiling every cpp file to an object file
$(OBJ_DIR)/%.o: $(SRC_DIR)/%.cpp
	$(CXX) $(CXXFLAGS) -I$(INC_DIR) -c $< -o $@

# Cleaning up the build files
clean:
	@echo "Cleaning up..."
	rm -rvf $(OBJ_DIR) $(BIN_DIR)

# Phony targets
.PHONY: all build clean
