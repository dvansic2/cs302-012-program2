### Compiling
Run `make` in the root dir

### Running
Run `bin/program2` to run the program.

#### NOTE:
Turtles have low intelligence and speed and therefore have the highest chance of being sent back to the start. The program will run until at least one turtle reaches the finish line. Depending on the random selection of obstacles, the program may run for a long time. This could be fixed by better tuning the stats and obstacles, but I dont have time for that.

#### NOTE 2:
The printing of the race status doesnt play nice with a ssh connection and will flicker a lot. It works fine in a local terminal. This can be fixed by increasing the PRINT_DELAY_MS in the event.cpp, but at the cost of increased runtime.

#### NOTE 3:
The race will run until at least one participant reaches the finish line, at which the race will end. The winner and the location of the rest of the participants will be printed. The program will then exit.